import binaryInterpreter as bi
import unittest

class someFunctionTests(unittest.TestCase):
    def testOne(self):
        self.assertTrue(bi.runTests())

def main():
    unittest.main()

if __name__ == '__main__':
    main()
