# Binary Interpreter
Small C# Binary Interpreter

This program will be C# based, however I find python an easier language to prototype in so therefore the stable python version is currently in master until a stable version of the C# version is developed.

---

## Build Status

| Branch | Status |
| :----: | :----: |
| Master | [![Build Status](https://travis-ci.org/ProfessorStupid/Binary-Interpreter.svg?branch=master)](https://travis-ci.org/ProfessorStupid/Binary-Interpreter) |
| Develop | [![Build Status](https://travis-ci.org/ProfessorStupid/Binary-Interpreter.svg?branch=develop)](https://travis-ci.org/ProfessorStupid/Binary-Interpreter) |
| Python | [![Build Status](https://travis-ci.org/ProfessorStupid/Binary-Interpreter.svg?branch=python-prototype)](https://travis-ci.org/ProfessorStupid/Binary-Interpreter) |
